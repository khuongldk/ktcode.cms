module.exports = ({ env }) => ({
  defaultConnection: "default",
  connections: {
    default: {
      connector: "bookshelf",
      settings: {
        client: "mysql",
        host: env("DATABASE_HOST", "localhost"),
        port: env.int("DATABASE_PORT", 3306),
        database: env("DATABASE_NAME", "chjrcogbhosting_kt_code_db"),
        username: env("DATABASE_USERNAME", "chjrcogbhosting_khuongld"),
        password: env("DATABASE_PASSWORD", "ABAuDDeL[c[."),
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});
