module.exports = ({ env }) => ({
  host: env("HOST", "0.0.0.0"),
  port: env.int("PORT", 3337),
  admin: {
    auth: {
      secret: env("ADMIN_JWT_SECRET", "fb2ed3588e25bca2e145b0a240980c68"),
    },
  },
});
